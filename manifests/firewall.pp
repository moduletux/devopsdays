# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include devopsdays::firewall
class devopsdays::firewall {
  class { 'firewall':
    ensure_v6 => 'stopped',
  }

  firewall { '100 allow http traffic':
    ensure   => 'present',
    provider => 'iptables',
    dport    => ['80', '443'],
    proto    => 'tcp',
    action   => 'accept',
  }
}
