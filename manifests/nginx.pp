# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include devopsdays::nginx
class devopsdays::nginx {
  class { 'nginx':
    confd_purge  => true,
    server_purge => true,
  }

  case $facts['os']['name'] {
    'Ubuntu': { contain devopsdays::nginx::ubuntu }
    /^(RedHat|CentOS)$/: { contain devopsdays::nginx::el }
    default: { fail("${facts['os']['name']} is not supported!") }
  }
}
