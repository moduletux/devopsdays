# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include devopsdays::php
class devopsdays::php {
  class { 'php':
    ensure       => 'present',
    manage_repos => true,
    fpm          => true,
    dev          => true,
    composer     => true,
    pear         => true,
    phpunit      => false,
    settings     => {
      'fpm/cgi.fix_pathinfo' =>  $devopsdays::fix_cgi,
    },
    extensions   => $devopsdays::php_extensions,
  }

  php::fpm::pool { 'example.com':
    listen       => $devopsdays::fpm_skt_dir,
    listen_owner => $devopsdays::fpm_listen_owner,
    listen_group => $devopsdays::fpm_listen_group,
    user         => $devopsdays::fpm_user,
    group        => $devopsdays::fpm_group,
  }
}
