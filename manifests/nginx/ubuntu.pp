# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include devopsdays::nginx::ubuntu
class devopsdays::nginx::ubuntu {
  $php_ver = $facts['os']['release']['major'] ? {
    '18.04' => '7.2',
    '16.04' => '7.0',
  }

  # file { '/var/www':
  #   ensure  => directory,
  #   require => Package['nginx'],
  # }
  # file { '/var/www/html':
  #   ensure  => directory,
  #   require => File['/var/www'],
  # }
  # 
  # file { '/var/www/html/info.php':
  #   ensure  => file,
  #   content => template('devopsdays/info.php'),
  #   require => File['/var/www/html'],
  #   notify  => Service['nginx'],
  # }
  file { '/usr/share/nginx/html/info.php':
    ensure  => file,
    content => template('devopsdays/info.php'),
    require => Package['nginx'],
    notify  => Service['nginx'],
  }

  nginx::resource::server { 'example.com':
    ensure               => 'present',
    listen_port          => 80,
    www_root             => '/usr/share/nginx/html',
    use_default_location => false,
    index_files          => [
      'index.php',
      'index.html',
      'index.htm',
    ],
    server_name          => ['example.com'],
  }

  nginx::resource::location { 'php_files_location':
    ensure        => 'present',
    server        => 'example.com',
    location      => '~ \.php$',
    www_root      => '/usr/share/nginx/html',
    fastcgi       => 'unix:/var/run/php/php-fpm.sock',
    fastcgi_param => {
      'SCRIPT_FILENAME' => '$document_root$fastcgi_script_name',
    },
    priority      => 501,
  }

  nginx::resource::location { 'php_deny':
    ensure              => 'present',
    server              => 'example.com',
    location            => '~ \/.ht',
    location_cfg_append => {
      'deny' => 'all',
    },
    priority            => 502,
  }
}
