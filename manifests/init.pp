# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include devopsdays
class devopsdays (
  Boolean $fix_cgi,
  Hash $php_extensions,
  String $fpm_listen_owner,
  String $fpm_listen_group,
  String $fpm_user,
  String $fpm_group,
  String $fpm_skt_dir,
) {
  contain devopsdays::firewall
  contain devopsdays::nginx
  contain devopsdays::mysql
  contain devopsdays::php
}
