require 'spec_helper'

describe 'devopsdays::mysql' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }
      it { is_expected.to contain_class('devopsdays::mysql') }
      it { is_expected.to contain_class('mysql::server') }
      it { is_expected.to contain_package('mysql-server').with_ensure('present') }
      it { is_expected.to contain_service('mysqld').with_ensure('running') }
    end
  end
end
