require 'spec_helper'

describe 'devopsdays::nginx::el' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:pre_condition) { 'include devopsdays::nginx' }

      if os_facts[:os]['name'] == 'Ubuntu'
        it { is_expected.not_to compile }
      else
        it { is_expected.to compile }
      end
    end
  end
end
