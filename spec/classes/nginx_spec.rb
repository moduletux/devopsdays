require 'spec_helper'

describe 'devopsdays::nginx' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }
      it { is_expected.to contain_class('devopsdays::nginx') }
      it { is_expected.to contain_class('nginx') }
      it { is_expected.to contain_package('nginx').with_ensure('present') }
      it { is_expected.to contain_service('nginx').with_ensure('running') }
    end
  end
end
